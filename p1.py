def do_factorial(num):
    factorial = 1
    while num > 1:
        factorial = factorial * num
        num -= 1

    return factorial

if __name__ == "__main__":
    for i in range(1,11):
        print("Factorial de " + str(i) + " es", str(do_factorial(i)))
